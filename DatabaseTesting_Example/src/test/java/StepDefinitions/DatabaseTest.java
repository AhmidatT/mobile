package StepDefinitions;

import Database.DatabaseConnection;
import Database.DatabaseStatements;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import utils.Constants;

import java.sql.*;
import java.util.*;

import static utils.Constants.*;

public class DatabaseTest {
    DatabaseConnection DBConnect = new DatabaseConnection();
    DatabaseStatements DBStatement = new DatabaseStatements();
    Constants constants = new Constants();
    Connection con;
    String ReturnValue_Query;
    ArrayList<String> ReturnValue_Query1;
    Integer ReturnValue_Query_Int;

    //Query to Execute-
    String Query_Employees = "SELECT COUNT(first_name) FROM EMPLOYEES;";



    @After
    public void Teardow() {
        DBStatement.DeleteEmployee(FIRST_NAME2, LAST_NAME2);
        DBStatement.VerifyEmployeeHasBeenRemoved(FIRST_NAME2, LAST_NAME2);
        DBConnect.disconnectFomDatabase();
    }


    @Given("I connect to database successfully")
    public void i_connect_to_database_successfully() {
        con = DBConnect.ConnectToDatabase();
    }

    @When("I run a query to get a count of employees")
    public void i_run_a_query_to_get_a_count_of_employees() throws SQLException {
        //   ReturnValue_Query = DBStatement.QueryDatabase(Query_Employees);
        ReturnValue_Query_Int = DBStatement.QueryDatabase_AndReturnIntegerValue(Query_Employees, 1);
    }


    @Then("The result should return <{int}> employees")
    public void the_result_should_return_employees(Integer no_employees) {
        DBStatement.VerifyCount(ReturnValue_Query_Int, no_employees);
        System.out.println(no_employees + ReturnValue_Query_Int);
    }

    @When("I run a query to search for the start date for {string} and {string}")
    public void i_run_a_query_to_search_for_the_start_date_for_and(String first_name, String last_name) throws SQLException {
        ReturnValue_Query = DBStatement.QueryDatabase(constants.QUERY_STARTDATE + constants.WHERE + constants.EMPLOYEE_FIRSTNAM_EEQUALS + first_name + AND + constants.EMPLOYEE_LASTNAME_EQUALS + last_name + SEPARATE_STATEMENTS);
    }

    @Then("The result should return a start date of {string}")
    public void the_result_should_return_a_start_date_of(String start_date) {
        DBStatement.VerifyString(start_date, ReturnValue_Query);
        System.out.print("actual" + ReturnValue_Query + "expected" + start_date);
    }

    @When("I run a query to search for the date of birth for {string} and {string}")
    public void i_run_a_query_to_search_for_the_date_of_birth_for_and(String first_name, String last_name) throws SQLException {
        System.out.println(constants.QUERY_DOB + constants.WHERE + constants.EMPLOYEE_FIRSTNAM_EEQUALS + first_name + AND + constants.EMPLOYEE_LASTNAME_EQUALS + last_name + SEPARATE_STATEMENTS);
        ReturnValue_Query = DBStatement.QueryDatabase(constants.QUERY_DOB + constants.WHERE + constants.EMPLOYEE_FIRSTNAM_EEQUALS + first_name + AND + constants.EMPLOYEE_LASTNAME_EQUALS + last_name + SEPARATE_STATEMENTS);
    }


    @When("I run a query to get the full list of employees in the {string} department")
    public void i_run_a_query_to_get_the_full_list_of_employees_in_the_department(String string) throws SQLException {
        ReturnValue_Query1 = (DBStatement.QueryDatabase_AndReturnAllValues(QUERY_EMPLOYEE_DEPT, FULL_EMPOLOYEE_NAME));
    }


    @Then("I confirm that my list of Employees are past of the Quality Management team")
    public void iConfirmThatMyListOfEmployeesArePastOfTheQualityManagementTeam(io.cucumber.datatable.DataTable EmployeesInQA) {
            DBStatement.CompareArrayList_with_DataTable(EmployeesInQA,"FULL_NAME", ReturnValue_Query1);
    }

    @When("I add a {string} to the employee table")
    public void iAddAToTheEmployeeTable(String new_employee) throws SQLException{
        DBStatement.InserNewEmployee(new_employee);
    }


    @When("I add add a {string} and confirm that the employee has been added")
        public void iAddAddAAndConfirmThatTheEmployeeHasBeenAdded(String new_employee, io.cucumber.datatable.DataTable NewEmployee) throws SQLException{
        DBStatement.InserNewEmployee(new_employee);
        ReturnValue_Query = (DBStatement.GetEmployeeFullName(FIRST_NAME3, LAST_NAME3));
        DBStatement.CompareSingleValue_with_DataTable(NewEmployee, "employee", ReturnValue_Query);
    }


    @Then("Confirm that the employee is present in the database")
    public void confirm_that_the_employee_is_present_in_the_database(io.cucumber.datatable.DataTable NewEmployee) {
        ReturnValue_Query = (DBStatement.GetEmployeeFullName(FIRST_NAME2, LAST_NAME2));
        DBStatement.CompareSingleValue_with_DataTable(NewEmployee, "employee", ReturnValue_Query);
    }

    @Then("When I delete the employee from the Table")
    public void when_I_delete_the_employee_from_the_Table() {
        DBStatement.DeleteEmployee(FIRST_NAME3, LAST_NAME3);
    }

    @Then("I confirm that the employee has been successfully removed")
    public void i_confirm_that_the_employee_has_been_successfully_removed() {
        DBStatement.VerifyEmployeeHasBeenRemoved(FIRST_NAME3, LAST_NAME3);

    }

    @When("I add a {string} with an existing EmployeeID to the employee table")
    public void iAddAWithAnExistingEmployeeIDToTheEmployeeTable(String new_employee) throws SQLException {
        DBStatement.InserNewEmployee_CatchDuplicate(new_employee);
    }
}