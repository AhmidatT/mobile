package Database;

import java.sql.*;
import java.util.Properties;

import static utils.Constants.*;

public class DatabaseConnection {
    // init connection object
    private Connection connection;
    // init properties object
    private Properties properties;


    private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }



  /*  public Connection ConnectToDatabase(String DatabaseURL, String Username, String Password ) throws SQLException {
        Connection con = DriverManager.getConnection(DatabaseURL, Username, Password);
        return con;
    }*/

    // connect database
    public Connection ConnectToDatabase() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }




    //disconnect from database
    public void disconnectFomDatabase() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
