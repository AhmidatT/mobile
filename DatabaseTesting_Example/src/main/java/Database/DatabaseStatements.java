package Database;


import io.cucumber.datatable.DataTable;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static utils.Constants.*;

public class DatabaseStatements  extends DatabaseConnection {

   /* public Statement createStatement() throws SQLException {
    Statement stmqt = con.createStatement();
    return stmt;
    }*/

    public String QueryDatabase(String sqlStatement) throws SQLException {
        //This will only return the value from the first cell in the the first column
        // try {
        PreparedStatement statement = ConnectToDatabase().prepareStatement(sqlStatement);
        ResultSet resultSet = statement.executeQuery(sqlStatement);
        resultSet.next();
        String result = resultSet.getString(1);
        return result;
    }


    public Integer QueryDatabase_AndReturnIntegerValue(String sqlStatement, Integer columnIndex) throws SQLException {
        //This will only return the value from the first cell in the first column
        // try {
        PreparedStatement statement = ConnectToDatabase().prepareStatement(sqlStatement);
        ResultSet resultSet = statement.executeQuery(sqlStatement);
        resultSet.next();
        //  Integer result = resultSet.getString(1);
        //This will only return the value from a column index starts from 1 upwards
        Integer result = resultSet.getInt(columnIndex);
        return result;
    }

    public String GetEmployeeFullName(String first_name, String last_name) {
        try {
            PreparedStatement statement = ConnectToDatabase().prepareStatement(SELECT_FROM_EMPLOYEE_TABLE_WITH_FIRST_LAST_NAME);
            statement.setString(1, first_name);
            statement.setString(2, last_name);
            System.out.println("new statement" + statement + "new statement");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            String result = resultSet.getString(1);
            return result;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<String> QueryDatabase_AndReturnAllValues(String sqlStatement, String ColumnName) {
        try {
            PreparedStatement statement = ConnectToDatabase().prepareStatement(sqlStatement);
            ResultSet resultSet = statement.executeQuery(sqlStatement);
            ArrayList<String> Nos = new ArrayList<String>();
            while (resultSet.next()) {
                Nos.add(resultSet.getString(ColumnName));
            }
            //  System.out.print("TEST>" +Nos +"TEST");
            return Nos;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void VerifyCount(Integer ExpectedCount, Integer ActualCount) {
        try {
            Assert.assertEquals(ExpectedCount, ActualCount);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    public void VerifyString(String ExpectedStringValue, String ActualStringValue) {
        try {
            Assert.assertEquals(ExpectedStringValue, ActualStringValue);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Integer sumValue() {

        return null;
    }


    public List ChangeString_ToList(String string) {
        //Splits the list by comma
        String str = string;
        List<String> newList = new ArrayList<>(Arrays.asList(str.split(",")));
        return newList;
    }

    public void CompareArrayList_with_DataTable(io.cucumber.datatable.DataTable datatable, String DatatableColumn_Name, ArrayList<String> List) {
        String Newstr = String.valueOf(List);
        for (Map<Object, Object> data : datatable.asMaps(String.class, String.class)) {
            Object datatableColumn = data.get(DatatableColumn_Name);
            Assert.assertThat(Newstr, CoreMatchers.containsString(String.valueOf(datatableColumn)));
        }
    }

    public void CompareSingleValue_with_DataTable(DataTable dataTable, String DatatableColumn_Name, String Value) {
        String Newstr = Value;
        for (Map<Object, Object> data : dataTable.asMaps(String.class, String.class)) {
            Object datatableColumn = data.get(DatatableColumn_Name);
            Assert.assertThat(Newstr, CoreMatchers.containsString(String.valueOf(datatableColumn)));
        }

    }


    public void InserNewEmployee(String values) throws SQLException {
        PreparedStatement statement = ConnectToDatabase().prepareStatement(INSERT_INTO_EMPLOYEE_TABLE + values);
        System.out.println("AT statement" + statement + "AT statement");
        boolean resultSet = statement.execute(INSERT_INTO_EMPLOYEE_TABLE + values);
    }

    public void InserNewEmployee_CatchDuplicate(String values) throws SQLException {
        PreparedStatement statement = ConnectToDatabase().prepareStatement(INSERT_INTO_EMPLOYEE_TABLE + values);
        try {
            statement.execute(INSERT_INTO_EMPLOYEE_TABLE + values);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public void DeleteEmployee(String first_name, String last_name) {
        // It firsts fetches the employeeID using the first_name and last_name of the employee
        try {
            PreparedStatement statement = ConnectToDatabase().prepareStatement(STATEMENT_GET_EMPLOYEE_ID);
            statement.setString(1, first_name);
            statement.setString(2, last_name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String employeeID = resultSet.getString(1);
                //Deletes from the employee table using the employeeID
                PreparedStatement statement_del = ConnectToDatabase().prepareStatement(STATEMENT_DELETE_EMPLOYEE);
                statement_del.setString(1, employeeID);
                statement_del.execute();
                resultSet.next();
            }

        } catch (SQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void VerifyEmployeeHasBeenRemoved(String first_name, String last_name) {
        try {
            PreparedStatement statement = ConnectToDatabase().prepareStatement(STATEMENT_GET_EMPLOYEE_ID);
            statement.setString(1, first_name);
            statement.setString(2, last_name);
            ResultSet resultSet = statement.executeQuery();
           // resultSet.next();
            if (!resultSet.next()) {
                System.out.println("No data");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}