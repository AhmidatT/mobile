package utils;

import java.sql.PreparedStatement;

public class Constants {

    // init database Constants
    public static final String DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/ahmidat_test";
   // public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "Taiwo4eva";
    public static final String MAX_POOL = "3000"; // set your own limit


    public static final String WHERE = "WHERE ";
    public static final String AND = "AND ";
    public static final String EMPLOYEE_FIRSTNAM_EEQUALS = "first_name =";
    public static final String EMPLOYEE_LASTNAME_EQUALS = "last_name = ";
    public static final String SEPARATE_STATEMENTS = ";";




    //Employee Table

    public static final String EMPLOYEE_COLUMN_FIRST_NAME = "first_name";
    public static final String EMPLOYEE_COLUMN_LAST_NAME = "last_name";
    public static final String EMPLOYEE_COLUMN_EMP_NO= "emp_no";
    public static final String EMPLOYEE_COLUMN_GENDER = "gender";
    public static final String EMPLOYEE_COLUMN_HIREDATE = "hire_date";
    public static final String EMPLOYEE_COLUMN_BIRTHDATE = "birth_date";

    //Department Table

    public static final String DEPARTMENET_COLUMN_DEP_NO = "dept_no";
    public static final String DEPARTMENET_COLUMN_DEPT_NAME = "dept_name";


    // Department Employee Table

    public static final String DEPARTMENETEMPLOYEE_COLUMN_EMP_NO = "emp_no";
    public static final String DEPARTMENETEMPLOYEE_COLUMN_DEPT_NO = "dept_no";
    public static final String DEPARTMENETEMPLOYEE_COLUMN_FROM = "from_date";
    public static final String DEPARTMENETEMPLOYEE_COLUMN_TO = "to_date";


    //Department Managers Table

    public static final String DEPARTMENETMANAGERS_COLUMN_DEPT_NO = "dept_no";
    public static final String DEPARTMENETMANAGERS_COLUMN_EMP_NO = "emp_no";
    public static final String DEPARTMENETMANAGERS_COLUMN_FROM = "from_date";
    public static final String DEPARTMENETMANAGERS_COLUMN_TO = "to_date";


    //Offices Table

    public static final String OFFICES_COLUMN_OFFICE_CODE = "officeCode";
    public static final String OFFICES_COLUMN_CITY = "city";
    public static final String OFFICES_COLUMN__PHONE = "phone";
    public static final String OFFICES_COLUMN_ADDRESS_LN_1 = "addressLine1";
    public static final String OFFICES_COLUMN_ADDRESS_LN_2 = "addressLine2";
    public static final String OFFICES_COLUMN__STATE = "state";
    public static final String OFFICES_COLUMN_COUNTRY = "country";
    public static final String OFFICES_COLUMN_POSTCODE = "postalCode";
    public static final String OFFICES_COLUMN_TERRITORY = "territory";

    //Salaries Table

    public static final String SALARIES_COLUMN_EMP_NO = "emp_no";
    public static final String SALARIES_COLUMN_SALARY = "salary";
    public static final String SALARIES_COLUMN_FROM = "from_date";
    public static final String SALARIES_COLUMN_TO = "to_date";

    // Titles Table

    public static final String TITLE_COLUMN_EMP_NO = "emp_no";
    public static final String TITLE_COLUMN_titile = "title";
    public static final String TITLE_COLUMN_FROM = "from_date";
    public static final String TITLE_COLUMN_TO = "to_date";

    //Current_dept_emp View

    public static final String CURRENT_DEPT_EMP_COLUMN_EMP_NO = "emp_no";
    public static final String CURRENT_DEPT_EMP_COLUMN_DEPT_NO ="dept_no";
    public static final String CURRENT_DEPT_EMP_COLUMN_FROM = "from_date";
    public static final String CURRENT_DEPT_EMP_COLUMN_TO = "to_date";

    // dept_emp_latest_date

    public static final String CURRENT_DEPT_EMP_LATEST_COLUMN_EMP_NO = "emp_no";
    public static final String CURRENT_DEPT_EMP_LATEST_COLUMN_FROM = "from_date";
    public static final String CURRENT_DEPT_EMP_LATEST_COLUMN_TO  = "to_date";


    //Custom Column Names
    public static final String FULL_EMPOLOYEE_NAME = "FULLNAME";

    //Prepared statements

   public static final String INSERT_INTO_EMPLOYEE_TABLE =  "INSERT INTO `employees` VALUES ";
   public static final String SELECT_FROM_EMPLOYEE_TABLE_WITH_FIRST_LAST_NAME =  "SELECT CONCAT(E.first_name, ' ', E.last_name) AS FULLNAME FROM `employees` E WHERE first_name = ? and Last_name = ?; ";
   public static final String QUERY_STARTDATE = "SELECT hire_date FROM EMPLOYEES ";
   public static final String QUERY_DOB ="SELECT birth_date FROM EMPLOYEES ";
   public static final String QUERY_EMPLOYEE_DEPT = "SELECT CONCAT(E.first_name, ' ', E.last_name) AS FULLNAME \n" +
         "\tFROM `employees` E\n" +
         "\tJOIN dept_emp DE ON DE.emp_no = E.emp_no\n" +
         "    JOIN departments D ON D.dept_no = DE.dept_no\n" +
         "    WHERE dept_name = 'Quality Management' ;";
   public static final String STATEMENT_DELETE_EMPLOYEE = "DELETE FROM `employees` WHERE emp_no = ?;";
   public static final String STATEMENT_GET_EMPLOYEE_ID = "SELECT emp_no FROM `employees` WHERE first_name = ? and Last_name = ?;";

   //Names
   public static final String FIRST_NAME1 = "Ahmidat";
   public static final String LAST_NAME1 = "Tijani";
   public static final String FIRST_NAME2 = "Claudio";
   public static final String LAST_NAME2 = "Di Folco";
   public static final String FIRST_NAME3 = "Amy";
   public static final String LAST_NAME3 = "Ward";


// PreparedStatement stmt=con.prepareStatement("update emp set name=? where id=?");






}
