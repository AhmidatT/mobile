Feature: Database Testing Feature

  Scenario: User is able get the total count of employees
    Given I connect to database successfully
    When I run a query to get a count of employees
    Then The result should return <1099> employees



  Scenario Outline: User is able to successfully get the start date of an employee
    Given I connect to database successfully
    When I run a query to search for the start date for "<first_name>" and "<last_name>"
    Then The result should return a start date of "2019-01-03"
    Examples:
      | first_name | last_name |
      | 'Ahmidat' | 'Tijani' |



  Scenario Outline: User is able to successfully get the date of birth of an employee
    Given I connect to database successfully
    When I run a query to search for the date of birth for "<first_name>" and "<last_name>"
    Then The result should return a start date of "1990-05-17"
    Examples:
      | first_name | last_name |
      | 'Ahmidat' | 'Tijani' |


  Scenario: Able to compare all employees available in the database
    Given I connect to database successfully
    When I run a query to get the full list of employees in the "Quality Management" department
    Then I confirm that my list of Employees are past of the Quality Management team
      | FULL_NAME |
      |Ahmidat Tijani|
      |Saniya Kalloufi|
      |Duangkaew Piveteau|
      |Karsten Joslin|
      |Alain Chappelet|
      |Hironoby Sidou|
      |Kshitij Gils|
      |Guoxiang Ramsay|
      |Zissis Pintelas|
      |Itzchak Lichtner|
      |Samphel Siegrist|
      |Herbert Trachtenberg|
      |Badri Furudate|
      |Vishu Strehl|
      |Kish Fasbender|
      |Shirish Wegerle|
      |Nalini Kawashimo|
      |Marek Luck|
      |Dipayan Seghrouchni|
      |Prodip Schusler|
      |Heon Ranai|
      |Pradeep Kaminger|
      |Marl Grospietsch|
      |Mikhail Rosis|
      |Yagil DasSarma|
      |Ortrud Nitto|
      |Martien Improta|
      |Hisao Tiemann|


    Scenario Outline: Successfully update add a new employee to the employee database
      Given I connect to database successfully
      When I add a "<new_employee>" to the employee table
      Then Confirm that the employee is present in the database
      | employee |
      | Claudio Di Folco |

      Examples:
        | new_employee |
        | (1100,'1995-07-20','Claudio','Di Folco','M','2018-03-17'); |



  Scenario Outline: I cannot add a new employee with an existing EmployeeID
    Given I connect to database successfully
    When I add a "<new_employee>" with an existing EmployeeID to the employee table
    #Then Confirm that it is
    Examples:
      | new_employee |
      | (1,'1995-07-20','Floral','Lavir','f','2018-03-17'); |




  Scenario Outline: Successfully Delete an employee
    Given I connect to database successfully
    When I add add a "<new_employee>" and confirm that the employee has been added
      | employee |
      | Amy Ward |
   # Then When I delete the employee from the Table
   # And  I confirm that the employee has been successfully removed
    Examples:
      | new_employee |
      | (1101,'1995-07-20','Amy','Ward','F','2018-03-17'); |

